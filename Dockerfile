FROM alpine:edge
MAINTAINER Volodymyr Riazantsev <volodymyr.riazantsev@globallogic.com>

RUN apk update && apk upgrade && \
    apk add --update  bash rsync jq openssh python git curl

# Install Google's "repo" tool and make it executable
RUN curl http://storage.googleapis.com/git-repo-downloads/repo > /usr/local/bin/repo
RUN chmod +x /usr/local/bin/repo

# Do initial git configuration in order to prevent repo fails
RUN git config --global user.name "John Doe" && \
    git config --global user.email "jd@umbrellacorp.com" && \
    git config --global color.ui false

COPY ./assets/* /opt/resource/
RUN  chmod +x /opt/resource/*
