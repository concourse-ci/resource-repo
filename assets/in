#!/bin/sh

set -e
set -x

exec 3>&1
exec 1>&2

source $(dirname $0)/common.sh

PATH=/usr/local/bin:$PATH

destination=$1

if [ -z "$destination" ]; then
  echo "usage: $0 <path/to/destination>" >&2
  exit 1
fi

payload=$(mktemp $TMPDIR/repo-resource-in.XXXXXX)

cat > $payload <&0

load_pubkey $payload
configure_git_ssl_verification $payload
configure_credentials $payload

uri=$(jq -r '.source.uri // ""' < $payload)
branch=$(jq -r '.source.branch // ""' < $payload)
manifest=$(jq -r '.source.manifest // ""' < $payload)
rjobs=$(jq -r '.source.jobs // ""' < $payload)
ref=$(jq -r '.version.ref // "HEAD"' < $payload)

if [ -z "$uri" ]; then
  echo "invalid payload (missing uri):" >&2
  cat $payload >&2
  exit 1
fi

bf=''
if [ ! -z "$branch" ]; then
    bf='-b '
fi

mf=''
if [ ! -z "$manifest" ]; then
    mf='-m '
fi

jf=''
if [ ! -z "$rjobs" ]; then
    jf='-j '
fi

cd $destination
repo init -u $uri --no-repo-verify $bf $branch $mf $manifest
repo sync $jf $rjobs

cd .repo/manifests
ref=$(git rev-parse --short HEAD)

jq -n "{
    version: {ref: $(echo $ref | jq -R .)}
}" >&3
